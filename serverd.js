/** @param {NS} ns **/
/** @param {import(".").NS } ns */


export async function main(ns) {
// takes a server name args[0], ram args[1] and waits until
// the player has enough available money to purchase it.
    let done = false;
    let targetFunds = ns.getPurchasedServerCost(ns.args[1]);

    while (!done) {
        let funds = ns.getPlayer().money;

        if ( funds >= targetFunds ) {
            ns.purchaseServer(ns.args[0], ns.args[1]);
            ns.tprint("Purchased server " + ns.args[0] + " with "
                + ns.nFormat(ns.args[1], '0.00b') + '.');
            ns.toast("Server Purchased!");
            done = true;
        
            // Copy files listed in files.txt to new server
            // unless args[2] is set to true
            if ( !ns.args[2] ) {
                ns.run('replicate.js', 1, "--dst", ns.args[0]);
            
            }

        } else {
            ns.asleep(60000);
        
        }

    }


}