/** @param {NS} ns **/
/** @param {import(".").NS } ns */

import * as hlib from "./hacklibs.js";

export async function main(ns) {

	while (true) {

		let player = ns.getPlayer();
		let server = ns.getServer(ns.args[0]);
		if (await hlib.CanPwn(ns, player, server)) {
			await hlib.Crack(ns, server);

		} else if (await hlib.CanWeaken(ns, server)) {
			await hlib.Weaken(ns, server);

		} else if (await hlib.CanHack(ns, server)) {
			await hlib.HackServ(ns, server);

		} else if (await hlib.CanGrow(ns, server)) {
			await hlib.GrowStash(ns, server);

		} else {
			await hlib.HackServ(ns, server);

		}

		await ns.sleep(5000);

	}

}