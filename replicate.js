/** @param {NS} ns **/
/** @param {import(".").NS } ns */
export async function main(ns) {

    let opts = ns.flags([
        ['file', 'files.txt'],
        ['src', 'home'],
        ['dst', ''],
        ['help', false],
    ]);

    if (opts['help'] || opts['dst'] == '') {
        help(ns);
    } else {
        let files = ns.read(opts['file']).split(',');
        await ns.scp(files, opts['src'], opts['dst']);

    }



}


export async function help(ns) {

    ns.tprint(`
    This program copies all files specified in a given filename from a source host to a destination host.
    
    Options:
    --file <filename>       Optional - Defaults to "files.txt"
    --src <server>          Optional - Defaults to "home"
    --dst <server>          Required - Destination server`);

}