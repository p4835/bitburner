/** @param {import(".").ns } ns */


function FundsAvailable(cash, cost, reserve = 1) {
	// returns a boolean value if the player has sufficient funds available.
	// may use 'reserve' to specify a percentage of player funds to hold back.
	let funds = cash * reserve;
	return (cost <= funds);

}


function NewNode(ns, userMoney) {

	// let newNodeCost = ns.hacknet.getPurchaseNodeCost();

	// get things rolling if it's a new start
	if (ns.hacknet.numNodes() < 1) {
		ns.print("No hacknet nodes found.\nPurchasing initial node...\n");
		return (ns.hacknet.purchaseNode());

	} else {
		let newNum = ns.hacknet.purchaseNode();
		ns.toast("Purchased new hacknet node, #" + newNum, "success");
		return (newNum);

	}

}


function FindUpgrade(ns) {

	let totalNodes = ns.hacknet.numNodes();
	let lowestCostIndex = 0;
	let cheapestUpgrade = 0;

	for (let i = totalNodes - 1; i >= 0; i--) {

		let levelUpCost = ns.hacknet.getLevelUpgradeCost(i);
		let ramUpCost = ns.hacknet.getRamUpgradeCost(i);
		let coreUpCost = ns.hacknet.getCoreUpgradeCost(i);
		let cheapestAvailable = Math.min(levelUpCost, ramUpCost, coreUpCost);

		if (cheapestUpgrade == 0) {
			cheapestUpgrade = cheapestAvailable;
			lowestCostIndex = i;
		} else if (cheapestAvailable < cheapestUpgrade) {
			cheapestUpgrade = cheapestAvailable;
			lowestCostIndex = i;
		}

	}

	let bestOption = {
		"index": lowestCostIndex,
		"levelUpCost": ns.hacknet.getLevelUpgradeCost(lowestCostIndex),
		"ramUpCost": ns.hacknet.getRamUpgradeCost(lowestCostIndex),
		"coreUpCost": ns.hacknet.getCoreUpgradeCost(lowestCostIndex)
	};

	return bestOption;

}


function PurchaseUgrade(ns, index, cheapestAvailable) {

	let levelUpCost = ns.hacknet.getLevelUpgradeCost(index);
	let ramUpCost = ns.hacknet.getRamUpgradeCost(index);
	let coreUpCost = ns.hacknet.getCoreUpgradeCost(index);


	if (levelUpCost == cheapestAvailable) {
		let result = ns.hacknet.upgradeLevel(index);
		if (result) {
			ns.toast("Purchased Level Upgrade for Node " + index, "success");
		}
	} else if (ramUpCost == cheapestAvailable) {
		let result = ns.hacknet.upgradeRam(index);
		if (result) {
			ns.toast("Purchased RAM Upgrade for Node " + index, "success");
		}

	} else if (coreUpCost == cheapestAvailable) {
		let result = ns.hacknet.upgradeCore(index);
		if (result) {
			ns.toast("Purchased Core Upgrade for Node " + index, "success");
		}
	}

}

export async function main(ns) {
    /** @param {ns} ns **/

    // main loop
    while (true) {

        let user = ns.getPlayer();
        let userMoney = user.money;
        let reserve = 0.5;
        // let nodesRemaining = ns.hacknet.maxNumNodes() - ns.hacknet.numNodes();

        if (FundsAvailable(userMoney, ns.hacknet.getPurchaseNodeCost(), reserve)) {
            NewNode(ns, userMoney);

        }

        let nextUpgrade = FindUpgrade(ns);
        let nextUpgradeCost = Math.min(nextUpgrade["levelUpCost"],
            nextUpgrade["ramUpCost"],
            nextUpgrade["coreUpCost"]);
        if (FundsAvailable(userMoney, nextUpgradeCost, reserve)) {
            PurchaseUgrade(ns, nextUpgrade["index"], nextUpgradeCost);
        }

        if (FundsAvailable(userMoney, nextUpgradeCost, reserve)) {
            await ns.sleep(10);
        } else {
            await ns.sleep(30000);
        }

    }

}