/** @param {NS} ns **/
/** @param {import(".").NS } ns */

/*
pull.js
===================
Pulls updated files from gitlab. URL needs to be changed in source if a
different server/repo is desired.

Optional Arguments:
===================
--branch    Specify the branch to pull from. Defaults to 'testing'
--file      Specify a specific file to pull. Leave blank to pull everything.`)
*/

export async function main(ns) {
    ns.tail();

    let opts = ns.flags([
        ['branch', 'testing'],
        ['file', 'pull.js'],
    ])
    let urlBase = 'https://gitlab.com/p4835/bitburner/-/raw/';
    let branch = opts['branch'] + '/';
    let file = opts['file'];
    let updated = await ns.readPort(20) == 1 ? true : false;

    if ( !updated ) {
        // update pull.js first and relaunch
        await ns.writePort(20, 1);
        ns.tprint('Updating from remote repository, please wait...');
        await ns.wget(urlBase + 'testing/' + file, file);
        ns.spawn(file, 1, '--branch', opts['branch'], '--file', opts['file']);
    
    } else if ( updated ) {
        file = 'repolist.txt';
        await ns.wget(urlBase + branch + file, file);
        if ( opts['file'] == 'pull.js' ) {
            // Get file list and update all
            let files = ns.read(file).split('\n');
 
            for (let i = 0; i < files.length; i++) {
                const f = files[i];
                await ns.wget(urlBase + branch + f, f);

                }
                
            } else {
                // Get the specified file
                await ns.wget(urlBase + branch + opts['file']);

            }

    }


}