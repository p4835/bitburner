/** @param {NS} ns **/
/** @param {import(".").NS } ns */

// Take a server and player object, and return true if hackable
// false if not.
export function CanPwn(ns, player, server) {

	let playerTools = 0;
	if (ns.fileExists("BruteSSH.exe", "home")) {
		++playerTools;
	}
	if (ns.fileExists("FTPCrack.exe", "home")) {
		++playerTools;
	}
	if (ns.fileExists("relaySMTP.exe", "home")) {
		++playerTools;
	}
	if (ns.fileExists("HTTPWorm.exe", "home")) {
		++playerTools;
	}
	if (ns.fileExists("SQLInject.exe", "home")) {
		++playerTools;
	}

	if (server.hasAdminRights) {
		return false;
	} else if ((player.hacking >= server.hackDifficulty) &&
		(server.numOpenPortsRequired <= playerTools)) {
		return true;
	} else {
		return false;

	}

}


// Take a given server and hack it.
export async function Crack(ns, server) {

	if (!server.sshPortOpen) {
		if (ns.fileExists("BruteSSH.exe", "home")) {
			await ns.brutessh(server.hostname);
		} else {
			ns.print("BruteSSH.exe missing from home server, unable to open SSH port.");
			ns.toast("Error Hacking " + server.hostname + ": BruteSSH.exe Missing.", "error");
			return 1;
		}
	}

	if (!server.ftpPortOpen) {
		if (ns.fileExists("FTPCrack.exe", "home")) {
			await ns.ftpcrack(server.hostname);
		} else {
			ns.print("FTPCrack.exe missing from home server, unable to open FTP port.");
			ns.toast("Error Hacking " + server.hostname + ": FTPCrack.exe Missing.", "error");
			return 2;
		}
	}

	if (!server.smtpPortOpen) {
		if (ns.fileExists("relaySMTP.exe", "home")) {
			await ns.relaysmtp(server.hostname);
		} else {
			ns.print("relaySMTP.exe missing from home server, unable to open SMTP port.");
			ns.toast("Error Hacking " + server.hostname + ": relaySMTP.exe Missing.", "error");
			return 3;
		}
	}

	if (!server.httpPortOpen) {
		if (ns.fileExists("HTTPWorm.exe", "home")) {
			await ns.httpworm(server.hostname);
		} else {
			ns.print("HTTPWorm.exe missing from home server, unable to open http port.");
			ns.toast("Error Hacking " + server.hostname + ": HTTPWorm.exe Missing.", "error");
			return 4;
		}
	}

	if (!server.sqlPortOpen) {
		if (ns.fileExists("SQLInject.exe", "home")) {
			await ns.sqlinject(server.hostname);
		} else {
			ns.print("SQLInject.exe missing from home server, unable to open SQL port.");
			ns.toast("Error Hacking " + server.hostname + ": SQLInject.exe Missing.", "error");
			return 5;
		}
	}

	await ns.nuke(server.hostname);

	if (server.hasAdminRights) {
		ns.toast("Server " + server.hostname + " has been rooted.", "success");
	} else {
		ns.toast("Error: Unable to root server " + server.hostname + ".");
	}
}


export async function CanWeaken(ns, server) {

	let minSecLvl = server.minDifficulty;
	let currentSecLvl = server.hackDifficulty;
	if (currentSecLvl > minSecLvl * 1.1) {
		return true;

	} else {
		return false;

	}

}


export async function Weaken(ns, server) {

	let minSecLvl = server.minDifficulty;
	let curSecLvl = server.hackDifficulty;
	let secDiff = curSecLvl - minSecLvl;
	let threads = 1;
	let wknAmt = 0;
	let host = ns.getServer();
	let maxRamAvailable = host.maxRam - host.ramUsed;
	let scriptMem = ns.getScriptRam("weaken.js", "home");
	let maxThreads = host.hostname = 'home' ? Math.floor(maxRamAvailable / scriptMem) - 16 : Math.floor(maxRamAvailable / scriptMem);

	while (secDiff > wknAmt) {
		wknAmt = ns.weakenAnalyze(threads, host.cpuCores);
		if ((wknAmt >= secDiff) || (threads == maxThreads)) {
			ns.run("weaken.js", threads, server.hostname);
			// await ns.sleep(100);
			// ns.print(ns.getRunningScript("weaken.js", "home", server.hostname).logs[0]);
		} else {
			threads++;
		}
	}

}


export async function CanHack(ns, server) {

	let serverMinSec = server.minDifficulty;
	let serverDiff = Math.floor(server.hackDifficulty);

	if ((server.hackDifficulty <= server.requiredHackingSkill) &&
		(serverMinSec >= serverDiff) &&
		(server.moneyAvailable >= .9 * server.moneyMax)) {

		return true;

	} else {

		return false;

	}

}


export async function HackServ(ns, server) {

	let host = ns.getServer();
	let maxRamAvailable = host.maxRam - host.ramUsed;
	let scriptMem = ns.getScriptRam("hack.js", "home");
	let maxThreads = host.hostname = 'home' ? Math.floor(maxRamAvailable / scriptMem) - 16 : Math.floor(maxRamAvailable / scriptMem);
	let optimumThreads = ns.hackAnalyzeThreads(server.hostname, server.moneyAvailable);
	let threads = 0;

	if (optimumThreads > maxThreads) {
		threads = maxThreads;

	} else {
		threads = optimumThreads;

	}

	if (threads > 0) {
		ns.run("hack.js", threads, server.hostname)
		// await ns.sleep(100);
		// ns.print(ns.getRunningScript("hack.js", host.hostname, server.hostname).logs[0]);

	}

}


export async function CanGrow(ns, server) {

	if (server.moneyAvailable < 0.95 * server.moneyMax) {
		return true;

	} else {
		return false;

	}
}


export async function GrowStash(ns, server) {

	let host = ns.getServer();
	let maxRamAvailable = host.maxRam - host.ramUsed;
	let scriptMem = ns.getScriptRam("grow.js", "home");
	let maxThreads = host.hostname = 'home' ? Math.floor(maxRamAvailable / scriptMem) - 16 : Math.floor(maxRamAvailable / scriptMem);
	let growthFactor = (server.moneyMax / (server.moneyAvailable != 0 ? server.moneyAvailable : 1));
	let optimumThreads = Math.ceil(ns.growthAnalyze(server.hostname, growthFactor, host.cpuCores));
	let threads = 0;

	optimumThreads > maxThreads ? threads = maxThreads : threads = optimumThreads;

	if (threads > 0) {
		ns.run("grow.js", threads, server.hostname);

	}
	// // await ns.sleep(100);
	// ns.print(ns.getRunningScript("grow.js", host.hostname, server.hostname).logs[0]);

}