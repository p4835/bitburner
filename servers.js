/** @param {NS} ns **/
/** @param {import(".").NS } ns */


export async function help(ns) {

	ns.tprint(`
Server Utility Program Help
===========================
	
Option 			Description
------			-----------
--cost			Lists cost to purchase a server with x ram
--buy			Purchase a server.
			Format: --buy <server name>
			Ex: run servers.js --buy --name slave0 --ram 8
--delete		Delete a purchased server.
			Format: --delete <server name>
			Ex: run servers.js --delete --name slave0
--list			List all purchased servers.
--daemonize		Daemonize the purchasing process until funds
			are available.
			Ex: run servers.js --daemonize --name slave0
--name			Name of the target server.
			Format: --name <server name>
--ram			Optional. Amount of server ram to buy.
			Default = 1048576
--no-replicate		Do not copy over files from files.txt`)

	
}


export async function main(ns) {

	let opts = ns.flags([
		['cost', false],
		['buy', false],
		['delete', false],
		['list', false],
		['daemonize', false],
		['help', false],
		['name', 'slave'],
		['ram', 1048576],
		['no-replicate', false],
	]);

	if (opts['cost']) {
		for (let i = 1; i <= 20; i++) {
			ns.tprint(
				ns.nFormat(ns.getPurchasedServerCost(Math.pow(2, i)), '0.0a') +
				'\t= ' + ns.nFormat(Math.pow(2, i), '0,0') + "GB RAM");

		}

	} else if (opts['buy']) {
		ns.tprint('Buying Server Named ' + opts['name'] + ' with ' + opts['ram']
			+ "GB RAM...");
		ns.purchaseServer(opts['name'], opts['ram']);
		if (!opts['no-replicate']) {
			ns.run('replicate.js', 1, '--dst', opts['name']);

		}

	} else if (opts['delete']) {
		ns.tprint('Deleting Server Named ' + opts['name'] + '.');
		ns.deleteServer(opts['name']);

	} else if (opts['list']) {
		ns.tprint(ns.getPurchasedServers());

	} else if (opts['daemonize']) {
		ns.spawn('serverd.js', 1, opts['name'], opts['ram'], opts['no-replicate']);

	} else {
		help(ns);

	}


};

